angular.module('F1FeederApp.services', [])
    .factory('ergastAPIservice', function($http) {

        var ergastAPI = {};
        var saison = 2015;

        ergastAPI.getDrivers = function() {
            return $http({
                method: 'JSONP',
                url: 'http://ergast.com/api/f1/' + saison + '/driverStandings.json?callback=JSON_CALLBACK'
            });
        }

        ergastAPI.getDriverDetails = function(id) {
            return $http({
                method: 'JSONP',
                url: 'http://ergast.com/api/f1/' + saison + '/drivers/'+ id +'/driverStandings.json?callback=JSON_CALLBACK'
            });
        }

        ergastAPI.getDriverRaces = function(id) {
            return $http({
                method: 'JSONP',
                url: 'http://ergast.com/api/f1/' + saison + '/drivers/'+ id +'/results.json?callback=JSON_CALLBACK'
            });
        }

        return ergastAPI;
    });